WebJinks 
(http://bit.ly/webjinx)

-----Bit.ly Links-----
Mega File Shares
http://bit.ly/megajinx
- https://mega.nz/#F!q8sgnKxL!BPUsC0JmNPsZIOKFHqipHQ

Kodi Pastebin
http://bit.ly/kodijinx


------- Indigo and GitHub Browser --------

Username
mhancoc7

Kodi for fire stick
http:a//bit.ly/kodi182

------- Repositories ------

Kodil
https://github.com/kodil/kodil/tree/master/repo

Super repo
http://srp.nu/

All eyes on me
http://lvtvv.com/repo/
http://aeom.cf/repo/

Kodi Bae Repository
http://lazykodi.com/

Supremacy
http://supremacy.org.uk/zip/repo/

Seren Repo
https://nixgates.github.io/packages
Seren scrapers
http://bit.ly/a4kScrapers

13 Clowns
https://github.com/13Clowns/_zips/

DeathStar
http://miniaturelife67.co.uk/repo

OneNation (Deceit)
http://www.onenation.info

KNE Repo (Incursion & Placenta)
http://start.kodineuerleben.eu

No Limits
http://nolimitsbuilds.com/kodi/



---------- Configuration ---------
special://userdata/addon_data/plugin.video.exodus/TVShows

special://userdata/addon_data/plugin.video.genesisreborn/TVShows

special://userdata/addon_data/plugin.video.exodusredux/TVShows

--------- Links ------
https://pastebin.com/iLUX17A2
http://bit.do/kodijinx

-----------Kodi Build Links --------

https://docs.google.com/spreadsheets/d/19KNXDylokA-ImwsueMAxLsKRbMkexDt4AD2psBflBug/edit?usp=drivesdk



**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).